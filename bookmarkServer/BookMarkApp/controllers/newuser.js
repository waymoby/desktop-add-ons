var ACS = require('acs').ACS;

function add(req,res){

    var name        = req.body.name;
    var email       = req.body.email;
    var password    = req.body.password;
    var passwordConf    = req.body.passwordConf;
    var username    = req.body.username;

    ACS.Users.create({
        name: name,
        email : email,
        password : password,
        password_confirmation : passwordConf,
        username : username
    }, function(data) {
            console.log(data);
            res.redirect('/home');
    })

}
