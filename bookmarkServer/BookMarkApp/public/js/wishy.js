var el = new sharer();
	el.init();
var escapedVars;

function sharer(){

	var information 	= {};
	var self 			= this;
	var allImages 		= [];
	var iframe 			= '';
	var wishWindow 		= '';

	this.init = function(){
		self.makeFrame();
		self.selectImages();			
		loadJsCssFile("https://57756013ce7f3a0180deaaba76f233116a572c86.cloudapp.appcelerator.com/css/style.css", "css");
	}

	this.makeFrame = function() { 

		var div = create('div');
		div.className = "bookmarklet";

    	ifrm = create("iframe"); 
    	self.iframe = ifrm;
    	ifrm.setAttribute("src", "https://57756013ce7f3a0180deaaba76f233116a572c86.cloudapp.appcelerator.com/home"); 
    	ifrm.style.width = 250+"px"; 
    	ifrm.style.height = 275+"px"; 
    	var close = create('div');
    		close.className = "close_wishy";
    		close.onclick	= function(){
    			self.hide();
    		};
    	div.appendChild(close);
    	div.appendChild(ifrm);
    	document.body.appendChild(div); 
    	wishWindow = div;

	}

	this.selectImages = function(){
		var docImages = document.images;
		for (var i = 0; i < docImages.length; i++) {
			if(docImages[i].offsetWidth > 150 && docImages[i].offsetHeight > 150){
				allImages.push({ number : i, src : docImages[i].src });
			}
		}
		window.allWishyImages = allImages;
	
		self.iframe.setAttribute('src', "https://57756013ce7f3a0180deaaba76f233116a572c86.cloudapp.appcelerator.com/home#tags:"+JSON.stringify(allWishyImages)+"");

	}

	this.hide = function(){

		var el = wishWindow;
			el.parentNode.removeChild(el);

	}

}


function loadJsCssFile(filename, filetype){

	if (filetype=="js"){ 

		var fileref	=	document.createElement('script');
		fileref.setAttribute("type","text/javascript");
		fileref.setAttribute("src", filename);

	} else if (filetype=="css"){ 

		var fileref	=	document.createElement('link');
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", filename);

	}

	if (typeof fileref!="undefined")
		document.getElementsByTagName("head")[0].appendChild(fileref);
	}


function create(item){
	return document.createElement(item);
}

/* @@ Load script
function loadScript(url, callback)
{
	var head = document.getElementsByTagName("head")[0];
	var script = document.createElement("script");
	script.src = url;

	// Attach handlers for all browsers
	var done = false;
	script.onload = script.onreadystatechange = function()
	{
		if( !done && ( !this.readyState 
					|| this.readyState == "loaded" 
					|| this.readyState == "complete") )
		{
			done = true;

			// Continue your code
			callback();

			// Handle memory leak in IE
			script.onload = script.onreadystatechange = null;
			head.removeChild( script );
		}
	};

	head.appendChild(script);
}*/


/*

function imageData(i){
	var imgs = latest.images;
	data = [
		'total='+imgs.length,
		'idx='+i,
		'loc='+encodeURIComponent(location.protocol+'//'+location.host+location.pathname+location.search)
	];
	if(imgs[i]){
		data.push('src='+encodeURIComponent(imgs[i].src));
		data.push('title='+encodeURIComponent(imgs[i].getAttribute('alt') || imgs[i].getAttribute('title') || document.title));
	}
	return data.join('&');
}
*/
